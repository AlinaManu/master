import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import torchvision
from torchvision import datasets, transforms
from torch.autograd import Variable
from torchvision_update_emnist import EMNIST
from my_random_affine import RandomAffine
import pandas as pd

#input_size = 784 #28x28
#input_size = 76800 #240x320
input_size = 1200 #40*30

class Net(nn.Module):
    def __init__(self, hidden_units=[200], dropout=False):
        super(Net, self).__init__()
        
        self.dropout = dropout
        
        self.fc1 = nn.Linear(input_size, hidden_units[0])
        self.h_no = len(hidden_units) - 1
        for i in range(0, len(hidden_units) -1 ):
            if dropout:
                setattr(self, f"drop_h{i:d}", nn.Dropout())
            setattr(self, f"fc_h{i:d}",
                    nn.Linear(hidden_units[i], hidden_units[i+1]))
        if dropout:
            self.last_dropout = nn.Dropout()
        self.fc2 = nn.Linear(hidden_units[-1], 10)

    def forward(self, x):
        x = x.view(-1, input_size)
        x = F.relu(self.fc1(x))
        
        #print("DEGUB After f1:")
        #print(x)       
        
        for i in range(self.h_no):
            if self.dropout:
                x = getattr(self, f"drop_h{i:d}")(x)
            x = getattr(self, f"fc_h{i:d}")(x)
            x = F.relu(x)
        if self.dropout:
            x = self.last_dropout(x)
        x = self.fc2(x)
        
        #print("DEGUB After f2:")
        #print(x)
        
        return F.log_softmax(x, dim=1) #95%


class ConvNet(nn.Module):
    def __init__(self, hidden_units=[200], dropout=False):
        super(ConvNet, self).__init__()
        
        self.conv_net = nn.Sequential(
                nn.Conv2d(1, 16, 5, 1),
                nn.ReLU(),
                nn.MaxPool2d(2),
                nn.Conv2d(16, 16, 5, 1),
                nn.ReLU(),
                nn.MaxPool2d(2)
            )
        self.fc = nn.Sequential(nn.Linear(448, 10))

    def forward(self, x):
        x = self.conv_net(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return F.log_softmax(x, dim=1)
        
        
           
def get_loaders(args):
    resizeValue = (30, 30) #(h,w)
    paddingValue = 5
    kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data_mnist', train=True, download=True,
       #EMNIST('../data_mnist', 'letter', train=True, download=True,
                       transform=transforms.Compose([
                           #transforms.RandomRotation(20),
                           #RandomAffine(10, translate=(0.25, 0.25)),
                           RandomAffine(5, translate=(0.20, 0.20)),
                           transforms.Resize(resizeValue),
                           transforms.Pad((0, paddingValue)),
                           transforms.ToTensor(),
                           transforms.Lambda(lambda x: (x > .01).float()) if args.binarize else transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data_mnist', train=False, 
       #EMNIST('../data_mnist', 'letter', train=False, 
                           transform=transforms.Compose([
                           #transforms.RandomRotation(20),
                           #RandomAffine(10, translate=(0.25, 0.25)),
                           RandomAffine(5, translate=(0.20, 0.20)),
                           transforms.Resize(resizeValue),
                           transforms.Pad((0, paddingValue)),
                           transforms.ToTensor(),
                           transforms.Lambda(lambda x: (x > .01).float()) if args.binarize else transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.test_batch_size, shuffle=True, **kwargs)
    
    return train_loader, test_loader

def train(epoch, model, train_loader, optimizer, args):
    model.train()
    correct = 0
    train_loss = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        train_loss += loss.data.item()
        loss.backward()
        optimizer.step()
        if (batch_idx + 1) % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data[0]))
            
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).long().cpu().sum()
    
    train_loss /= len(train_loader.dataset)
    print('[Epoch {: 2d}] Train set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)'.format(
        epoch, train_loss, correct, len(train_loader.dataset),
        100. * correct / len(train_loader.dataset)))
    return 100. * correct / len(train_loader.dataset)

    
def test(epoch, model, test_loader, args):
    model.eval()
    test_loss = 0
    correct = 0
    for data, target in test_loader:
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        #data, target = Variable(data, volatile=True), Variable(target)
        with torch.no_grad():
            data, target = Variable(data), Variable(target)
        output = model(data)
        test_loss += F.nll_loss(output, target, size_average=False).data.item() # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).long().cpu().sum()

    test_loss /= len(test_loader.dataset)
    print('[Epoch {: 2d}] Test set : Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)'.format(
        epoch, test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))
    return 100. * correct / len(test_loader.dataset)


def run(args: argparse.Namespace):
    args.cuda = not args.no_cuda and torch.cuda.is_available()
    args.binarize = not args.no_binary
    args.dropout = bool(args.dropout)
    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)
    
    train_loader, test_loader = get_loaders(args)
    
    #model = ConvNet()
    model = Net(hidden_units=args.hidden_units, dropout=args.dropout)

    if args.cuda:
        model.cuda()
    Optimizer = optim.__dict__[args.optimizer]
    optimizer = Optimizer(model.parameters(), lr=args.lr)
    print(model)
    # Training
    train_accs = []
    test_accs = []
    for epoch in range(1, args.epochs + 1):
        train_accs.append(train(epoch, model, train_loader, optimizer, args))
        test_accs.append(test(epoch, model, test_loader, args))
        
    if args.save_net:
        torch.save(model.state_dict(), "Pipeline/pipeline_net.pt")
        dataiter = iter(test_loader)
        images, labels = dataiter.next()
        images_numpy = (Variable(images).data).cpu().numpy()
        np.save("Pipeline/pipeline_testloader", images_numpy)
        labels_numpy = (Variable(labels).data).cpu().numpy()
        np.save("Pipeline/pipeline_labels", labels_numpy)
    
    return train_accs, test_accs

def get_args(strict=False):
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=3000, metavar='N',
                        help='batch size for training default 3000')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='batch size for testing (default: 1000)')
    parser.add_argument('-e', '--epochs', type=int, default=30, metavar='N',
                        help='number of epochs to train (default: 30)')
    parser.add_argument('-o', '--optimizer', type=str, default="Adam",
                        help='optimizer')
    parser.add_argument('--lr', type=float, default=0.001, metavar='LR',
                        help='learning rate (default: 0.001)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=100, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--hidden-units', type=int, default=[50], nargs="*",
                        dest="hidden_units",
                        help='hidden units on hidden layers :) ')
    parser.add_argument('--no-binary', action="store_true", default=False,
                        dest="no_binary",
                        help='hidden units on hidden layers :) ')
    parser.add_argument('--dropout', default=0, dest="dropout",
                        help='use dropout')
    parser.add_argument('--save-net', action='store_true', default=False,
                        help='Save net')
    
    if strict:
        return parser.parse_args()
    
    args, _ = parser.parse_known_args()
    return args

def main():
    # Training settings
    args = get_args(strict=True)
    print("Run")
    train_accs, test_accs = run(args)
    
        
    #plt.plot(list(range(args.epochs)), train_accs, label="Train Accuracy")
    #plt.plot(list(range(args.epochs)), test_accs, label="Test Accuracy")
    #plt.legend(loc='best')
    #plt.legend(loc='upper left', bbox_to_anchor=(1, 1), prop={'size':10})
    #plt.tight_layout(pad=11)
    #plt.show()
    
    dfs = []
    title = "MNIST"
    df = pd.DataFrame({"epoch": list(range(1, args.epochs+1)), "accs": train_accs})
    df["title"] = title + " - train"
    dfs.append(df)
    df = pd.DataFrame({"epoch": list(range(1, args.epochs+1)), "accs": test_accs})
    df["title"] = title + " - test"
    dfs.append(df)
    full_df = pd.concat(dfs)
    full_df.to_pickle('results_dataframe_100.pkl')

    plt.figure(figsize=(10,10)) #width, height
    plt.plot(list(range(args.epochs)), train_accs, label="Train Accuracy")
    plt.plot(list(range(args.epochs)), test_accs, label="Test Accuracy")
    plt.xlabel("epochs")
    plt.ylabel("accs")
    plt.legend(loc='lower right')
    #plt.show()
    plt.savefig("results_plot_100.png", bbox_inches='tight')


if __name__ == "__main__":
    main()