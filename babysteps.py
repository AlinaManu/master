import pandas
from keras.models import Sequential
from keras import layers
from keras.preprocessing.text import Tokenizer
from sklearn.feature_extraction.text import CountVectorizer

from keras.preprocessing import sequence
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import Conv1D, GlobalMaxPooling1D
from keras.datasets import imdb

import matplotlib.pyplot as plt
plt.style.use('ggplot')

def plot_history(history):
	print("Plot")
	acc = history.history['acc']
	val_acc = history.history['val_acc']
	loss = history.history['loss']
	val_loss = history.history['val_loss']
	x = range(1, len(acc) + 1)
	plt.figure(figsize=(12, 5))
	plt.subplot(1, 2, 1)
	plt.plot(x, acc, 'b', label='Training acc')
	plt.plot(x, val_acc, 'r', label='Validation acc')
	plt.title('Training and validation accuracy')
	plt.legend()
	plt.subplot(1, 2, 2)
	plt.plot(x, loss, 'b', label='Training loss')
	plt.plot(x, val_loss, 'r', label='Validation loss')
	plt.title('Training and validation loss')
	plt.legend()
	plt.show()

def simple_nn(x_train, y_train, x_test, y_test):
	input_dim = x_train.shape[1]  # Number of features
	model = Sequential()
	model.add(layers.Dense(64, input_dim=input_dim, activation='relu'))
	model.add(layers.Dense(64, activation='relu'))
	model.add(layers.Dense(64, activation='relu'))
	model.add(layers.Dense(1, activation='sigmoid'))
	model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
	model.summary()

	history = model.fit(x_train, y_train,
	                    epochs=10,
	                    verbose=False,
	                    validation_data=(x_test, y_test),
	                    batch_size=300)
	return model, history

def convolutional_nn_bad(sentences_train, sentences_test, y_train, y_test):
	tokenizer = Tokenizer(num_words=5000)
	tokenizer.fit_on_texts(sentences_train)
	x_train = tokenizer.texts_to_sequences(sentences_train)
	x_test = tokenizer.texts_to_sequences(sentences_test)
	vocab_size = len(tokenizer.word_index) + 1

	embedding_dim = 100
	maxlen = 100

	model = Sequential()
	model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
	model.add(layers.Conv1D(128, 5, activation='relu'))
	model.add(layers.GlobalMaxPooling1D())
	model.add(layers.Dense(10, activation='relu'))
	model.add(layers.Dense(1, activation='sigmoid'))
	model.compile(optimizer='adam',
	              loss='binary_crossentropy',
	              metrics=['accuracy'])
	model.summary()
	history = model.fit(x_train, y_train,
                    epochs=10,
                    verbose=False,
                    validation_data=(x_test, y_test),
                    batch_size=1)
	return model, history	


def convolutional_nn(x_train, y_train, x_test, y_test):
	max_features = 3000
	#maxlen = 400
	maxlen = x_test.shape[1]
	batch_size = 32
	embedding_dims = 32
	filters = 64
	kernel_size = 3
	hidden_dims = 512
	epochs = 10
	print(x_train.shape[0], 'train sequences')
	print(x_test.shape[0], 'test sequences')

	print('Pad sequences (samples x time)')
	#x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
	#x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
	print('x_train shape:', x_train.shape)
	print('x_test shape:', x_test.shape)

	print('Build model...')
	model = Sequential()
	model.add(Embedding(max_features,
						embedding_dims,
						input_length=maxlen))
	#model.add(Dropout(0.2))
	model.add(Conv1D(filters,
					kernel_size,
					padding='valid',
					activation='relu',
					strides=1))
	model.add(GlobalMaxPooling1D())
	model.add(Dense(hidden_dims))
	#model.add(Dropout(0.2))
	model.add(Activation('relu'))
	model.add(Dense(1))
	model.add(Activation('sigmoid'))
	model.compile(loss='binary_crossentropy',
				optimizer='adam',
				metrics=['accuracy'])
	model.summary()
	history = model.fit(x_train, y_train,
				batch_size=batch_size,
				epochs=epochs,
				validation_data=(x_test, y_test))
	return model, history

### GET DATA ###

DATASET_TRAIN	= "haspeede_FB-train.tsv"		# 3000 rows
DATASET_TEST	= "haspeede_FB-reference.tsv"	# 1000 rows
data_train 	= pandas.read_csv(DATASET_TRAIN, names=['text', 'label'], sep="\t")
data_test 	= pandas.read_csv(DATASET_TEST,  names=['text', 'label'], sep="\t")

sentences_train = data_train['text'].values
y_train = data_train['label'].values

sentences_test = data_test['text'].values
y_test = data_test['label'].values

vectorizer = CountVectorizer()
vectorizer.fit(sentences_train)
x_train = vectorizer.transform(sentences_train)
x_test = vectorizer.transform(sentences_test)


### TRAIN ###

# model, history = simple_nn(x_train, y_train, x_test, y_test)
# model, history = convolutional_nn(sentences_train, sentences_test, y_train, y_test)
model, history = convolutional_nn(x_train, y_train, x_test, y_test)


### PRINT RESULT ###

loss, accuracy = model.evaluate(x_train, y_train, verbose=False)
print("Training Accuracy: {:.4f}".format(accuracy))
loss, accuracy = model.evaluate(x_test, y_test, verbose=False)
print("Testing Accuracy:  {:.4f}".format(accuracy))

plot_history(history)