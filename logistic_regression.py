import pandas
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression

DATASET_TRAIN	= "haspeede_FB-train.tsv"		# 3000 rows
DATASET_TEST	= "haspeede_FB-reference.tsv"	# 1000 rows
data_train 	= pandas.read_csv(DATASET_TRAIN, names=['text', 'label'], sep="\t")
data_test 	= pandas.read_csv(DATASET_TEST,  names=['text', 'label'], sep="\t")

sentences_train = data_train['text'].values
y_train = data_train['label'].values

sentences_test = data_test['text'].values
y_test = data_test['label'].values

# test data from train data
#sentences_train, sentences_test, y_train, y_test = train_test_split(
#    sentences, y, test_size=0.25, random_state=1000)

vectorizer = CountVectorizer()
vectorizer.fit(sentences_train)
x_train = vectorizer.transform(sentences_train)
x_test = vectorizer.transform(sentences_test)

classifier = LogisticRegression(solver='lbfgs')
classifier.fit(x_train, y_train)
score = classifier.score(x_test, y_test)
print('Accuracy : {:.4f}'.format(score))
